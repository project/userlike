<?php

/**
 * @file Userlike module.
 */

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;

const USERLIKE_CDN_WIDGETS_BASE_URL = 'https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/';
const USERLIKE_CDN_WIDGETS_LIBRARY_NAME = 'userlike-cdn-widgets';

/**
 * Implements hook_help().
 */
function userlike_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.userlike':
      return t('<a href=":url">Userlike</a> is a Live-Chat-Software provider. This module allows to add Userlike Live-Chat to your Drupal website without the requirement to touch code.', [':url' => 'https://www.userlike.com/']);
  }
}

/**
 * Implements hook_library_info_build().
 */
function userlike_library_info_build() {
  $config = Drupal::config('userlike.settings');
  $userlike_secret = $config->get('userlike_secret');
  $libraries = [];
  $libraries[USERLIKE_CDN_WIDGETS_LIBRARY_NAME] = [
    'header' => ($config->get('userlike_script_position') === 'header'),
    'js' => [
      _userlike_get_cdn_widgets_url($userlike_secret) => [
        'type' => 'external',
        'minified' => TRUE,
        'attributes' => [
          'type' => 'text/javascript',
          'id' => 'userlike-cdn-widgets',
          'charset' => 'UTF-8',
          'async' => TRUE,
        ],
      ],
    ],
  ];

  return $libraries;
}

/**
 * Implements hook_page_attachments_alter().
 */
function userlike_page_attachments_alter(array &$attachments) {
  if (_userlike_request_should_be_visible()) {
    // This is a tracked page, add the library:
    $libraryName = 'userlike/' . USERLIKE_CDN_WIDGETS_LIBRARY_NAME;
    $attachments['#attached']['library'][$libraryName] = $libraryName;
  }
}

/**
 * Returns true if the userlike secret is set (not empty).
 *
 * @param \Drupal\Core\Config\ImmutableConfig $config
 *   ImmutableConfig.
 *
 * @return bool
 *   True if userlike_secret is not empty.
 */
function _userlike_is_secret_configured(ImmutableConfig $config) {
  $userlike_secret = $config->get('userlike_secret');
  return !empty($userlike_secret);
}

/**
 * Determine if userlike should be visible on the current path.
 *
 * Based on visibility setting this function returns TRUE if the userlike widget
 * code should be added to the current page and otherwise FALSE.
 *
 * @param \Drupal\Core\Config\ImmutableConfig $config
 *   ImmutableConfig.
 *
 * @return bool
 *   True if script should be visible on path, else false.
 */
function _userlike_should_path_be_visible(ImmutableConfig $config, string $path) {
  $should_path_be_visible = &drupal_static(__FUNCTION__ . $path);

  // Cache tracking result if function is called more than once.
  if (!isset($should_path_be_visible)) {
    $visible_paths_mode = $config->get('userlike_paths_mode');
    $visible_paths = $config->get('userlike_paths');
    // Match path if necessary.
    if (!empty($visible_paths)) {
      // Convert path to lowercase. This allows comparison of the same path
      // with different case. Ex: /Page, /page, /PAGE.
      $pages = mb_strtolower($visible_paths);
      $path_alias = \Drupal::service('path_alias.manager')->getAliasByPath($path);
      if (empty($path_alias)) {
        $path_alias = mb_strtolower($path);
      }
      else {
        $path_alias = mb_strtolower($path_alias);
      }

      $page_match = \Drupal::service('path.matcher')->matchPath($path_alias, $pages) || (($path != $path_alias) && \Drupal::service('path.matcher')->matchPath($path, $pages));
      $path_include = $visible_paths_mode === 'include';
      $should_path_be_visible = !($page_match xor $path_include);
    } else {
      // Track all paths if none set:
      $should_path_be_visible = TRUE;
    }
  }
  return $should_path_be_visible;
}

/**
 * Determine if userlike should be visible for the given user role.
 *
 * Based on visibility setting this function returns TRUE if userlike
 * widget code should be added for the current role and otherwise FALSE.
 *
 * @param \Drupal\Core\Config\ImmutableConfig $config
 *   ImmutableConfig.
 *
 * @return bool
 *   True if script should be visible on user role, else false.
 */
function _userlike_should_role_be_visible(ImmutableConfig $config, AccountProxyInterface $user) {
  $should_role_be_visible = &drupal_static(__FUNCTION__ . $user->id());
  if (!isset($should_role_be_visible)) {

    // First we get the user's roles and the ones configured for userlike.
    $user_roles = $user->getRoles();
    $configured_roles = $config->get('userlike_roles');
    if (!empty($configured_roles)) {
      // Now we can get the roles present in both lists
      // and check if there are any.
      $matching_roles = array_intersect($user_roles, $configured_roles);
      $user_has_any_role = count($matching_roles) > 0;
      $role_include = $config->get('userlike_roles_mode') === 'include';
      $should_role_be_visible = !($user_has_any_role xor $role_include);
    }
    else {
      // Track all roles if none are selected:
      $should_role_be_visible = TRUE;
    }
  }

  return $should_role_be_visible;
}

/**
 * Determines if the chat should be visible on the current request.
 *
 * @return bool
 *   True if script should be visible on current request.
 */
function _userlike_request_should_be_visible() {
  $config = Drupal::config('userlike.settings');
  if (!_userlike_is_secret_configured($config)) {
    return FALSE;
  }

  // Check path visibility:
  $path = Drupal::service('path.current')->getPath();
  if (!_userlike_should_path_be_visible($config, $path)) {
    return FALSE;
  }

  // Check roles visibility:
  $user = Drupal::currentUser();
  if (!_userlike_should_role_be_visible($config, $user)) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Returns the Userlike widget CDN url.
 *
 * @param string $userlike_secret
 *   The userlike secret string.
 *
 * @return string
 *   The userlike widget URL.
 */
function _userlike_get_cdn_widgets_url($userlike_secret) {
  return USERLIKE_CDN_WIDGETS_BASE_URL . rawurlencode($userlike_secret) . '.js';
}
