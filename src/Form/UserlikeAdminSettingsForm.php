<?php

namespace Drupal\userlike\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Userlike settings configuration form.
 */
class UserlikeAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'userlike_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'userlike.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('userlike.settings');

    if (empty($config->get('userlike_secret'))) {
      $this->messenger()
        ->addWarning($this->t('You have not set your Userlike secret yet. To use Userlike, you need to have a Userlike account. <a href=":userlike_register_url" target="_blank">Register for a Userlike FREE plan or trial here</a>.', [':userlike_register_url' => 'https://www.userlike.com']));
    }

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];

    $form['general']['userlike_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Userlike Secret'),
      '#description' => $this->t('Enter your userlike secret. You can find it in your Userlike Account at "Live-Chat > Config > Widgets > Your Widget > Edit widget > Install > Login Details für Apps"'),
      '#default_value' => $config->get('userlike_secret'),
    ];

    // Scope.
    $form['general']['userlike_script_position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Script position'),
      '#options' => [
        'header' => $this->t('Header'),
        'footer' => $this->t('Footer') . ' (' . $this->t('Default') . ', ' . $this->t('Recommended') . ')',
      ],
      '#default_value' => $config->get('userlike_script_position'),
      '#description' => $this->t('Select where to place your Userlike code snippet in the DOM. We recommend to place it in the footer for faster page loading.'),
    ];

    // Visibility settings.
    $form['visibility-vertical-tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Visibility'),
      '#attached' => [
        'library' => [
          'userlike/userlike.admin',
        ],
      ],
    ];

    // Pages specific configurations.
    $form['visibility']['pages'] = [
      '#type' => 'details',
      '#title' => $this->t('Pages'),
      '#group' => 'visibility-vertical-tabs',
    ];

    $form['visibility']['pages']['userlike_paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#title_display' => 'invisible',
      '#default_value' => $config->get('userlike_paths'),
      '#description' => $this->t(
        "Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.",
        [
          '%blog' => '/blog',
          '%blog-wildcard' => '/blog/*',
          '%front' => '<front>',
        ]
      ),
      '#rows' => 10,
    ];

    $form['visibility']['pages']['userlike_paths_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Restricted to certain pages'),
      '#options' => [
        'include' => $this->t('Show only on the listed pages'),
        'exclude' => $this->t('Hide only on the listed pages'),
      ],
      '#default_value' => $config->get('userlike_paths_mode'),
    ];

    // Roles specific configurations.
    $form['visibility']['roles'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
      '#group' => 'visibility-vertical-tabs',
    ];

    $form['visibility']['roles']['userlike_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_role_names()),
      '#default_value' => $config->get('userlike_roles'),
      '#description' => $this->t('If none of the roles are selected, all users can see the Userlike widget.<br />If a user has any of the roles checked, that user will see the widget (or excluded, depending on the setting above).'),
    ];

    $form['visibility']['roles']['userlike_roles_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Restricted to certain roles'),
      '#options' => [
        'include' => $this->t('Show only for the selected roles'),
        'exclude' => $this->t('Hide only for the selected roles'),
      ],
      '#default_value' => $config->get('userlike_roles_mode'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!preg_match('/^\w{64,}$/', $form_state->getValue('userlike_secret')) || !strlen($form_state->getValue('userlike_secret')) === 64) {
      $form_state->setErrorByName('userlike_secret', $this->t('A valid userlike secret is a 64 character alphanumeric string.'));
    }

    // Trim some text area values.
    $form_state->setValue('userlike_secret', trim($form_state->getValue('userlike_secret')));
    $form_state->setValue('userlike_paths', trim($form_state->getValue('userlike_paths')));
    $form_state->setValue('userlike_roles', array_filter($form_state->getValue('userlike_roles')));

    // Verify that every path is prefixed with a slash,
    // but don't check for slashes if no paths configured.
    if (!empty($form_state->getValue('userlike_paths'))) {
      $pages = preg_split('/(\r\n?|\n)/', $form_state->getValue('userlike_paths'));
      foreach ($pages as $page) {
        if (strpos($page, '/') !== 0 && $page !== '<front>') {
          $form_state->setErrorByName('userlike_paths', $this->t('Path "@page" not prefixed with slash.', ['@page' => $page]));
          // Drupal forms show one error only.
          break;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('userlike.settings');

    if (
      $config->get('userlike_secret') != $form_state->getValue('userlike_secret')
      || $config->get('userlike_script_position') != $form_state->getValue('userlike_script_position')
    ) {
      // Clear library_info cache if userlike_secret or script position changed:
      Cache::invalidateTags(['library_info']);
    }

    $config
      ->set('userlike_secret', $form_state->getValue('userlike_secret'))
      ->set('userlike_script_position', $form_state->getValue('userlike_script_position'))
      // Pages.
      ->set('userlike_paths_mode', $form_state->getValue('userlike_paths_mode'))
      ->set('userlike_paths', $form_state->getValue('userlike_paths'))
      // Roles.
      ->set('userlike_roles_mode', $form_state->getValue('userlike_roles_mode'))
      ->set('userlike_roles', array_values(array_filter($form_state->getValue('userlike_roles'))))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
