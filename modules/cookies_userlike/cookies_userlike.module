<?php

/**
 * @file
 * Contains cookies_userlike.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\cookies\CookiesKnockOutService;

/**
 * Implements hook_help().
 */
function cookies_userlike_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the cookies_userlike module.
    case 'help.page.cookies_userlike':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Submodule of COOKiES to block Userlike based on COOKiES configuration.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_library_info_alter().
 */
function cookies_userlike_library_info_alter(&$libraries, $extension) {
  $config = Drupal::config('userlike.settings');
  $userlike_secret = $config->get('userlike_secret');
  if (empty($userlike_secret)) {
    return;
  }

  // Prevent loading of the JavaScript until consent is given.
  $doKo = CookiesKnockOutService::getInstance()->doKnockOut();
  $libraryName = USERLIKE_CDN_WIDGETS_LIBRARY_NAME;
  if ($doKo && isset($libraries[$libraryName])) {
    // We need a foreach as the key is dynamic.
    foreach ($libraries[$libraryName]['js'] as &$jsLibrary) {
      // Changing the type to text/plain prevents the script src from loading.
      // Will be back to text/javascript in JS if consent was given.
      $jsLibrary['attributes']['type'] = 'text/plain';
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function cookies_userlike_page_attachments(&$page) {
  $config = Drupal::config('userlike.settings');
  $userlike_secret = $config->get('userlike_secret');
  if (empty($userlike_secret)) {
    return;
  }

  // Attach js library that heals the knock-out:
  $doKo = CookiesKnockOutService::getInstance()->doKnockOut();
  if ($doKo) {
    $page["#attached"]["library"]['cookies_userlike/handle_knockout'] = 'cookies_userlike/handle_knockout';
  }
}
