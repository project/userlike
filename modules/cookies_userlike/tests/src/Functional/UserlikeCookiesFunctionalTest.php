<?php

namespace Drupal\Tests\cookies_userlike\FunctionalJavascript;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests some basic cookies_userlike functionalities.
 *
 * @group cookies_etracker
 */
class UserlikeCookiesFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'cookies',
    'userlike',
    'cookies_userlike',
    'test_page_test',
    'block',
  ];

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A test administrator.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The userlike dummy account key for testing.
   *
   * @var string
   */
  protected $userlikeSecret = '0000000000000000000000000000000000000000000000000000000000000000';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Use the test page as the front page.
    $this->config('system.site')->set('page.front', '/test-page')->save();
    // Set userlike secret:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    // Create users:
    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole($this->createAdminRole('administrator', 'administrator'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the script is in header and its type is "text/plain" before consent.
   */
  public function testScriptIsTextPlainBeforeConsentInHeader() {
    $session = $this->assertSession();
    // Set the required settings:
    $this->config('userlike.settings')->set('userlike_script_position', 'header')->save();
    // Place the Cookie UI Block:
    $this->placeBlock('cookies_ui_block');
    // Check script type before consent:
    $this->drupalLogout();
    // Clear caches, optherwise the cached script is returned:
    drupal_flush_all_caches();
    // @todo We need to check why this doesn't work:
    // Cache::invalidateTags(['library_info']);
    // We also tried using:
    // \Drupal::service('cache.config')->invalidateAll();
    // \Drupal::service('cache.discovery')->invalidateAll();
    // but even this fails sometimes
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $session->elementExists('css', 'head script#userlike-cdn-widgets');
    // Check if the block is placed:
    $session->elementExists('css', '#cookiesjsr');
    // Check if script is "konocked-out":
    $session->elementAttributeContains('css', 'script#userlike-cdn-widgets', 'type', 'text/plain');
  }

  /**
   * Tests the script is in footer and its type is "text/plain" before consent.
   */
  public function testScriptIsTextPlainBeforeConsentInFooter() {
    $session = $this->assertSession();
    // Assert if module "cookies_userlike" is enabled:
    $this->assertTrue(\Drupal::service('module_handler')->moduleExists('cookies_userlike'));
    // Set the required settings:
    $this->config('userlike.settings')->set('userlike_script_position', 'footer')->save();
    // Place the Cookie UI Block:
    $this->drupalPlaceBlock('cookies_ui_block', ['region' => 'content']);
    // Check script type before consent:
    $this->drupalLogout();
    // Clear caches, optherwise the cached script is returned:
    drupal_flush_all_caches();
    // @todo We need to check why this doesn't work:
    // Cache::invalidateTags(['library_info']);
    // We also tried using:
    // \Drupal::service('cache.config')->invalidateAll();
    // \Drupal::service('cache.discovery')->invalidateAll();
    // but even this fails sometimes
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $session->elementExists('css', 'body script#userlike-cdn-widgets');
    // Check if the block is placed:
    $session->elementExists('css', '#cookiesjsr');
    // Check if script is "konocked-out":
    $session->elementAttributeContains('css', 'script#userlike-cdn-widgets', 'type', 'text/plain');
  }

}
