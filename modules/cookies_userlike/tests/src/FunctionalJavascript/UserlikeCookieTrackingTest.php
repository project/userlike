<?php

namespace Drupal\Tests\cookies_userlike\FunctionalJavascript;

use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the cookies_userlike tracking.
 *
 * @group cookies_userlike
 */
class UserlikeCookieTrackingTest extends WebDriverTestBase {
  use BlockCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'cookies',
    'userlike',
    'cookies_userlike',
    'test_page_test',
    'block',
  ];

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A test administrator.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A regular authenticated user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The userlike dummy account key for testing.
   *
   * @var string
   */
  protected $userlikeSecret = '0000000000000000000000000000000000000000000000000000000000000000';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Use the test page as the front page.
    $this->config('system.site')->set('page.front', '/test-page')->save();
    // Set userlike secret:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    // Create users:
    $this->user = $this->drupalCreateUser();
    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole($this->createAdminRole('administrator', 'administrator'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the header script type before and after consent.
   */
  public function testUserlikeHeaderScriptExistsAfterConsent() {
    $session = $this->assertSession();
    // Set the required settings:
    $this->config('userlike.settings')->set('userlike_script_position', 'header')->save();
    // Place the Cookie UI Block:
    $this->placeBlock('cookies_ui_block');
    // Clear caches, optherwise the cached script is returned:
    drupal_flush_all_caches();
    // Check script type before consent:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'head script#userlike-cdn-widgets');
    $session->elementAttributeContains('css', 'head script#userlike-cdn-widgets', 'type', 'text/plain');
    // Fire consent script, accept all:
    $script = "var options = { all: true };
      document.dispatchEvent(new CustomEvent('cookiesjsrSetService', { detail: options }));";
    $this->getSession()->getDriver()->executeScript($script);
    // Check script type after consent:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'head script#userlike-cdn-widgets');
    // Check the script code is executed (no src="text/plain"):
    $session->elementNotExists('css', 'head script#userlike-cdn-widgets[type]');
    // Now the script should be loaded:
    $session->elementAttributeContains('css', 'head script#userlike-cdn-widgets', 'src', 'https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0000000000000000000000000000000000000000000000000000000000000000.js');
  }

  /**
   * Tests the footer script type before and after consent.
   */
  public function testUserlikeFooterScriptExistsAfterConsent() {
    $session = $this->assertSession();
    // Set the required settings:
    $this->config('userlike.settings')->set('userlike_script_position', 'footer')->save();
    // Place the Cookie UI Block:
    $this->placeBlock('cookies_ui_block');
    // Clear caches, optherwise the cached script is returned:
    drupal_flush_all_caches();
    // Check script type before consent:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'body script#userlike-cdn-widgets');
    $session->elementAttributeContains('css', 'body script#userlike-cdn-widgets', 'type', 'text/plain');
    // Fire consent script, accept all:
    $script = "var options = { all: true };
      document.dispatchEvent(new CustomEvent('cookiesjsrSetService', { detail: options }));";
    $this->getSession()->getDriver()->executeScript($script);
    // Clear caches, optherwise the cached script is returned:
    // drupal_flush_all_caches();
    // Check script type after consent:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'body script#userlike-cdn-widgets');
    // Check the script code is executed (no src="text/plain"):
    $session->elementNotExists('css', 'body script#userlike-cdn-widgets[type]');
    // Now the script should be loaded:
    $session->elementAttributeContains('css', 'body script#userlike-cdn-widgets', 'src', 'https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0000000000000000000000000000000000000000000000000000000000000000.js');
  }

}
