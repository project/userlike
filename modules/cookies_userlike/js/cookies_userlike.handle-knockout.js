/**
 * @file
 * COOKiES Knock-Out handling for cookies_userlike submodule.
 */
(function (Drupal) {
  'use strict';

  /**
   * Define defaults.
   */
  Drupal.behaviors.cookies_userlike_handle_knockout = {
    // id corresponding to the cookies_service.schema->id.
    id: 'userlike',

    activate: function (context, settings) {
      // Heal the knock-out:
      var scriptIds = [
        'userlike-cdn-widgets'
      ];
      for (var i in scriptIds) {
        var script = document.getElementById(scriptIds[i]);
        if (script) {
          var content = script.innerHTML;
          var newScript = document.createElement('script');
          var attributes = Array.from(script.attributes);
          for (var attr in attributes) {
            var name = attributes[attr].nodeName;
            if (name == 'type') {
              continue;
            }
            newScript.setAttribute(name, attributes[attr].nodeValue);
          }
          newScript.innerHTML = content;
          script.parentNode.replaceChild(newScript, script);
        }
      }
    },

    fallback: function (context, settings) {
      // Do stuff here to display that 3rd-party service is disabled.
    },

    attach: function (context, settings) {
      var self = this;
      document.addEventListener('cookiesjsrUserConsent', function (event) {
        var service = (typeof event.detail.services === 'object') ? event.detail.services : {};
        if (typeof service[self.id] !== 'undefined' && service[self.id]) {
          self.activate(context, settings);
        } else {
          self.fallback(context, settings);
        }
      });
    }
  };
})(Drupal);
