/**
 * @file
 * userlike admin behaviors.
 */

(function ($) {

  'use strict';

  /**
   * Provide the summary information for the settings vertical tabs.
   */
  Drupal.behaviors.userlikeSettingsSummary = {
    attach: function () {
      // Make sure this behavior is processed only if drupalSetSummary is defined.
      if (typeof jQuery.fn.drupalSetSummary === 'undefined') {
        return;
      }

      $('#edit-pages').drupalSetSummary(function (context) {
        var $radio = $('input[name="userlike_paths_mode"]:checked', context);
        if ($radio.val() === 'exclude') {
          if (!$('textarea[name="userlike_paths"]', context).val()) {
            return Drupal.t('Not restricted');
          }
          else {
            return Drupal.t('All pages with exceptions');
          }
        }
        else {
          return Drupal.t('Restricted to certain pages');
        }
      });

      $('#edit-roles').drupalSetSummary(function (context) {
        var vals = [];
        $('input[type="checkbox"]:checked', context).each(function () {
          vals.push($.trim($(this).next('label').text()));
        });
        if (!vals.length) {
          return Drupal.t('All roles');
        }
        else if ($('input[name="userlike_roles_mode"]:checked', context).val() === 'exclude') {
          return Drupal.t('Excepted: @roles', {'@roles': vals.join(', ')});
        }
        else {
          return vals.join(', ');
        }
      });
    }
  };

})(jQuery);
