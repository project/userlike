#Userlike

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Userlike is a Live-Chat-Software provider. This module allows to add
Userlike Live-Chat to your Drupal website without the requirement to touch code.

REQUIREMENTS
-------------

* This module requires no modules outside of Drupal core.

INSTALLATION
------------

Install the module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------

* Navigate to Administration > Extend and enable the module.
* Navigate to Administration > Configuration > Userlike Live-Chat settings to
add Userlike Secret and Visibility settings.
* Navigate to Administration > People > permissions > Userlike Live-Chat Widget
Integration for permissions settings.

FURTHER TIPS & JS API
-------------
**Start the chat by clicking a button on the website**

To start the chat using a button on the website, you can use the following JavaScript snippet:
~~~
userlike.userlikeStartChat();
~~~

Inspect the global "userlike" object in your browser console for other public API functions like:
- `userlike.userlikeQuitChat()`
- `userlike.userlikeRequestOperatorChat()`
- `userlike.userlikeShowButton()` & `userlike.userlikeHideButton()`
- `userlike.enableDebug()` & `userlike.disableDebug()`
- `userlike.deleteCookies()`


*Note that these are not added by the module, but come from the Userlike Widget JavaScript itself.*

**Further widget API functions**
See here: https://www.userlike.com/de/public/tutorial/um/api/javascript/messenger-api


MAINTAINERS
-----------

* Julian Pustkuchen - https://www.drupal.org/u/anybody
* Joshua Sedler - https://www.drupal.org/u/grevil
