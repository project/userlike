<?php

namespace Drupal\Tests\userlike\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods for testing the userlike browser functionalities.
 *
 * @group userlike
 */
class UserlikeFunctionalTest extends BrowserTestBase {
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'userlike',
    'node',
    'test_page_test',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * An admin user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminuser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The userlike dummy account key for testing.
   *
   * @var string
   */
  protected $userlikeSecret = '0000000000000000000000000000000000000000000000000000000000000000';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create content type page:
    $this->createContentType(['type' => 'article']);

    // Use the test page as the front page.
    $this->config('system.site')->set('page.front', '/test-page')->save();
    // Create an user:
    $this->user = $this->drupalCreateUser([
      'administer userlike',
      'bypass node access',
      'access content',
      'access content overview',
    ]);
    // Create an admin user:
    $this->adminuser = $this->drupalCreateUser(['administer userlike']);
    $this->adminuser->addRole($this->createAdminRole('administrator', 'administrator'));
    $this->adminuser->save();
    // Login as regular authenticated user by default:
    $this->drupalLogin($this->user);
  }

  /**
   * Tests if the form is unaccessible as an anonymous user.
   */
  public function testUserlikeAccessFormAsAnonymous() {
    $this->drupalLogout();
    $session = $this->assertSession();
    // Go to settings and see if they exist:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(403);
  }

  /**
   * Tests if the form is accessible as an admin user.
   */
  public function testUserlikeAccessFormAsAdmin() {
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    $session = $this->assertSession();
    // Go to settings and see if they exist:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests saving the secret in form to configuration.
   */
  public function testUserlikeFormSecret() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to settings and see if they exist:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Fill the account key:
    $page->fillField('edit-userlike-secret', $this->userlikeSecret);
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The configuration options have been saved.');
    // Check if the account key is set in the config:
    $this->assertEquals($this->userlikeSecret, \Drupal::config('userlike.settings')->get('userlike_secret'));
  }

  /**
   * Tests saving the secret in form to configuration.
   */
  public function testUserlikeWrongFormSecret() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to settings and see if they exist:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Fill the account key with a wrong key:
    $page->fillField('edit-userlike-secret', 'This is not allowed as secret!');
    $page->pressButton('edit-submit');
    // Form still returns 200 on form error:
    $session->statusCodeEquals(200);
    // But page returns an error:
    $session->pageTextContains('A valid userlike secret is a 64 character alphanumeric string.');
  }

  /**
   * Tests if the userlike script exists in the header.
   */
  public function testUserlikeInHeaderExists() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    // See if account key is the same as in the config:
    $this->assertEquals($this->userlikeSecret, $this->config('userlike.settings')->get('userlike_secret'));
    $this->config('userlike.settings')->set('userlike_script_position', 'header')->save();
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    // Check parent is head:
    $this->assertSession()->elementExists('css', 'head script#userlike-cdn-widgets');
    // But not in body:
    $this->assertSession()->elementNotExists('css', 'body script#userlike-cdn-widgets');
    // By default, anonymous users should have the same result.
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    // Check parent is head:
    $this->assertSession()->elementExists('css', 'head script#userlike-cdn-widgets');
    // But not in body:
    $this->assertSession()->elementNotExists('css', 'body script#userlike-cdn-widgets');
  }

  /**
   * Tests if the userlike script exists in the footer.
   */
  public function testUserlikeInFooterExists() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $this->config('userlike.settings')->set('userlike_script_position', 'footer')->save();
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Got to front-page:
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    // Only check if parent element is body:
    $this->assertSession()->elementExists('css', 'body script#userlike-cdn-widgets');
    // Should not be present in head:
    $this->assertSession()->elementNotExists('css', 'head script#userlike-cdn-widgets');
    // By default, anonymous users should see the same script.
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    // Check parent is body:
    $this->assertSession()->elementExists('css', 'body script#userlike-cdn-widgets');
    // Should not be present in head:
    $this->assertSession()->elementNotExists('css', 'head script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is not loaded, including a specific path with a slash.
   */
  public function testExclusionByPathWithSlashUrl() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Create node1:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    // Don't show on node1:
    $page->fillField('edit-userlike-paths-mode-exclude', 'exclude');
    $page->fillField('edit-userlike-paths', '/node/1');
    $page->pressButton('edit-submit');
    // See if script won't load for our authenticated user:
    $this->drupalGet('/node/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // By default, anonymous users should have the same result.
    $this->drupalLogout();
    $this->drupalGet('/node/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is loaded, including a specific path with a slash.
   */
  public function testInclusionByPathWithSlashUrl() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Create node1:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    // Don't show on node1:
    $page->fillField('edit-userlike-paths-mode-include', 'include');
    $page->fillField('edit-userlike-paths', '/node/1');
    $page->pressButton('edit-submit');
    // See if script won't load for our authenticated user:
    $this->drupalGet('/node/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Script should NOT exist on other paths:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->drupalGet('/node/2');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // By default, anonymous users should have the same result.
    $this->drupalLogout();
    $this->drupalGet('/node/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is not loaded, including a specific path without slash.
   */
  public function testExclusionByPathWithAliasUrl() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Create node1:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Don't show on node1:
    $page->fillField('edit-userlike-paths-mode-exclude', 'exclude');
    $page->fillField('edit-userlike-paths', '<front>');
    $page->pressButton('edit-submit');
    // See if script won't load for our authenticated user:
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // Script should exist on other paths:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->drupalGet('/node/2');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // By default, anonymous users should have the same result.
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // Script should exist on other paths, also for anonymous:
    $this->drupalGet('/node/2');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is loaded, including a specific path without a slash.
   */
  public function testInclusionByPathWithAliasUrl() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Create node1:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Don't show on node1:
    $page->fillField('edit-userlike-paths-mode-include', 'include');
    $page->fillField('edit-userlike-paths', '<front>');
    $page->pressButton('edit-submit');
    // Script should exist on the included path:
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Script should NOT exist on any other path:
    $this->createNode(['type' => 'article', 'title' => 'Second node']);
    $this->drupalGet('/node/2');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // By default, anonymous users should have the same result.
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Script should NOT exist on any other path also for anonymous:
    $this->drupalGet('/node/2');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is not loaded on an authenticated user.
   */
  public function testExclusionByRoleAuthenticatedTest() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Show all roles except authenticated user:
    $page->fillField('edit-userlike-roles-mode-exclude', 'exclude');
    $page->fillField('edit-userlike-roles-authenticated', 'authenticated');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // See if script won't load for our authenticated user:
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // Anonymous users should still "see" the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is loaded on an authenticated user.
   */
  public function testInclusionByRoleAuthenticatedTest() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Only show authenticated user:
    $page->fillField('edit-userlike-roles-mode-include', 'include');
    $page->fillField('edit-userlike-roles-authenticated', 'authenticated');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // See if script will load for our authenticated user:
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Anonymous users shouldn't see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is not loaded on an authenticated user.
   */
  public function testExclusionByRoleAuthenticatedMultipleTest() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Esclude all roles (anonymous and authenticated):
    $page->fillField('edit-userlike-roles-mode-exclude', 'exclude');
    $page->fillField('edit-userlike-roles-authenticated', 'authenticated');
    $page->fillField('edit-userlike-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // See if script won't load for our authenticated user:
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // Anonymous users also shouldn't see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is loaded on an authenticated user.
   */
  public function testInclusionByRoleAuthenticatedMultipleTest() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Only show authenticated user:
    $page->fillField('edit-userlike-roles-mode-include', 'include');
    $page->fillField('edit-userlike-roles-authenticated', 'authenticated');
    $page->fillField('edit-userlike-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // See if script will load for our authenticated user:
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Anonymous users also should see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is not loaded on an anonymous user.
   */
  public function testExclusionByRoleAnonymousTest() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Show all roles except anonymous user:
    $page->fillField('edit-userlike-roles-mode-exclude', 'exclude');
    $page->fillField('edit-userlike-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Logout and see if script won't load for a an anonymous user:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // Authenticated users should still see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is loaded on an anonymous user.
   */
  public function testInclusionByRoleAnonymousTest() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Only show anonymous user:
    $page->fillField('edit-userlike-roles-mode-include', 'include');
    $page->fillField('edit-userlike-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Logout and see if script will load for an anonymous user:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Authenticated users shouldn't see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is not loaded on an anonymous user.
   */
  public function testExclusionByRoleAnonymousMultipleTest() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Show all roles except anonymous user:
    $page->fillField('edit-userlike-roles-mode-exclude', 'exclude');
    $page->fillField('edit-userlike-roles-authenticated', 'authenticated');
    $page->fillField('edit-userlike-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Logout and see if script won't load for a an anonymous user:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // Authenticated users also shouldn't see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is loaded on an anonymous user.
   */
  public function testInclusionByRoleAnonymousMultipleTest() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Only show anonymous user:
    $page->fillField('edit-userlike-roles-mode-include', 'include');
    $page->fillField('edit-userlike-roles-authenticated', 'authenticated');
    $page->fillField('edit-userlike-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Logout and see if script will load for an anonymous user:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Authenticated users should also see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is not loaded on an admin user.
   */
  public function testExclusionByRoleAdminTest() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Show all roles except admin user:
    $page->fillField('edit-userlike-roles-mode-exclude', 'exclude');
    $page->fillField('edit-userlike-roles-administrator', 'administrator');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Check if anonymous users still see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $session->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Check if authenticated users still see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $session->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    // Admin user shouldn't see the script:
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $session->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is loaded on an admin user.
   */
  public function testInclusionByRoleAdminTest() {
    $this->drupalLogout();
    // Login as adminuser:
    $this->drupalLogin($this->adminuser);
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Only show admin user:
    $page->fillField('edit-userlike-roles-mode-include', 'include');
    $page->fillField('edit-userlike-roles-administrator', 'administrator');
    $page->fillField('edit-userlike-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Check if anonymous users still see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Check if authenticated users still see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    // Admin user shouldn't see the script:
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is not loaded on an admin user.
   */
  public function testExclusionByRoleAdminMultipleTest() {
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Show all roles except admin user:
    $page->fillField('edit-userlike-roles-mode-exclude', 'exclude');
    $page->fillField('edit-userlike-roles-administrator', 'administrator');
    $page->fillField('edit-userlike-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Check if anonymous users don't see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // Check if authenticated users still see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $session->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    // Admin user shouldn't see the script:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the script is loaded on an admin user.
   */
  public function testInclusionByRoleAdminMultipleTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Only show admin user:
    $page->fillField('edit-userlike-roles-mode-include', 'include');
    $page->fillField('edit-userlike-roles-administrator', 'administrator');
    $page->fillField('edit-userlike-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Anonymous users should see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Authenticated users shouldn't see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    // Admin user shouldn't see the script:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests inherited role behaviour from authenticated to administrator.
   */
  public function testInclusionByRoleAuthenticatedInheritTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    // Go to userlike settings page:
    $this->drupalGet('/admin/config/services/userlike');
    $session->statusCodeEquals(200);
    // Only show admin user:
    $page->fillField('edit-userlike-roles-mode-include', 'include');
    $page->fillField('edit-userlike-roles-authenticated', 'authenticated');
    $page->pressButton('edit-submit');
    // Create node and set as frontpage:
    $this->createNode(['type' => 'article', 'title' => 'test123']);
    $this->config('system.site')->set('page.front', '/node/1')->save();
    // Anonymous users shouldn't see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'script#userlike-cdn-widgets');
    // Authenticated users still see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    // Admin user shouldn't see the script:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'script#userlike-cdn-widgets');
  }

  /**
   * Tests if the correct userlike script is loaded.
   */
  public function testUserlikeScriptAfterConsent() {
    $session = $this->assertSession();
    // Set account key:
    $this->config('userlike.settings')->set('userlike_secret', $this->userlikeSecret)->save();
    // Check script type after consent:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', 'script#userlike-cdn-widgets', 'src', 'https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0000000000000000000000000000000000000000000000000000000000000000.js');
  }

}
